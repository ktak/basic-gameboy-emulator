#include "GUI.h"

int main(int argc, char **argv)
{
    // Contains all of the gameboy state
	gb_state Game;

	if(argc != 2)
	{
	    fprintf(stderr, "Program requires command line argument\n");
	    exit(3);
	}

	gb_init(&Game, argv[1]);

	return GUI_run(&Game, argv[1]);
}
